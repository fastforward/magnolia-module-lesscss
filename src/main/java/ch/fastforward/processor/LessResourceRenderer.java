package ch.fastforward.processor;

import info.magnolia.context.MgnlContext;
import info.magnolia.freemarker.FreemarkerHelper;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.wrapper.HTMLEscapingNodeWrapper;
import info.magnolia.module.resources.ResourcesModule;
import info.magnolia.module.resources.loaders.ResourceLoader;
import info.magnolia.module.resources.renderers.ResourcesTextTemplateRenderer;
import info.magnolia.module.resources.templates.ResourceTemplate;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderException;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.RenderableDefinition;
import info.magnolia.rendering.util.AppendableWriter;
import org.apache.commons.io.IOUtils;
import org.lesscss.LessCompiler;
import org.lesscss.LessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zinggpa
 */
public class LessResourceRenderer extends ResourcesTextTemplateRenderer {
    private static Logger log = LoggerFactory.getLogger(ResourcesTextTemplateRenderer.class);

    @Inject
    public LessResourceRenderer(FreemarkerHelper fmRenderer, RenderingEngine renderingEngine) {
        super(fmRenderer, renderingEngine);
    }

    protected void onRender(Node content, RenderableDefinition definition, RenderingContext renderingCtx, Map<String, Object> ctx, String templateScript) throws RenderException {
        ResourceTemplate resourceTemplate = (ResourceTemplate) definition;
        HttpServletResponse response = MgnlContext.getWebContext().getResponse();
        String contentType = resourceTemplate.getContentType();
        boolean processed = resourceTemplate.isProcessed();
        response.setContentType(contentType);
        content = NodeUtil.deepUnwrap(content, HTMLEscapingNodeWrapper.class);
        StringBuffer text;
        if (this.shouldBypass(content)) {
            InputStream e = null;
            List reader = ResourcesModule.getInstance().getResourceLoaders();
            Iterator e1 = reader.iterator();

            while (e1.hasNext()) {
                ResourceLoader loader = (ResourceLoader) e1.next();

                try {
                    e = loader.getStream(templateScript);
                    if (e != null) {
                        break;
                    }
                } catch (IOException var26) {
                    log.debug("Can\'t load resource \'{}\' with ResourceLoader \'{}\'", templateScript, loader.getClass());
                }
            }

            if (e == null) {
                throw new RenderException(String.format("Template \'%s\' not found.", new Object[]{templateScript}));
            }

            try {
                text = new StringBuffer();
                text.append(IOUtils.toString(e));
            } catch (IOException var24) {
                throw new RenderException(String.format("Can\'t render resource \'%s\'.", new Object[]{templateScript}), var24);
            } finally {
                IOUtils.closeQuietly(e);
            }
        } else {
            text = new StringBuffer();
            text.append(PropertyUtil.getString(content, "text"));
        }

        try {
            AppendableWriter e2 = renderingCtx.getAppendable();
            if (processed) {
                try {
                    LessCompiler lessCompiler = new LessCompiler();
                    String result = lessCompiler.compile(text.toString());
                    e2.write(result);
                } catch (LessException e) {
                    log.error("Could not compile less resource [{}]", "");
                }
            } else {
                e2.write(text.toString());
            }

        } catch (IOException var23) {
            throw new RenderException("Can\'t render resource", var23);
        }
    }
}
