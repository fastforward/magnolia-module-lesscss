package ch.fastforward.processor;

import info.magnolia.module.templatingkit.functions.STKTemplatingFunctions;
import info.magnolia.module.templatingkit.resources.STKResourceModel;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.template.RenderableDefinition;
import info.magnolia.templating.functions.TemplatingFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;

/**
 * @author zinggpa
 */
public class LessResourceModel extends STKResourceModel {
    private static Logger log = LoggerFactory.getLogger(LessResourceModel.class);

    public LessResourceModel(Node content, RenderableDefinition definition, RenderingModel parent, STKTemplatingFunctions stkFunctions, TemplatingFunctions templatingFunctions) {
        super(content, definition, parent, stkFunctions, templatingFunctions);
    }
}
