package ch.fastforward.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.Task;
import info.magnolia.module.resources.setup.InstallBinaryResourcesTask;
import info.magnolia.module.resources.setup.InstallResourcesTask;
import info.magnolia.module.resources.setup.InstallTextResourcesTask;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manager the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class LessCssModuleVersionHandler extends DefaultModuleVersionHandler {

    protected List<Task> getBasicInstallTasks(InstallContext installContext) {
        ArrayList<Task> basicInstallTasks = new ArrayList<>();
        basicInstallTasks.addAll(super.getBasicInstallTasks(installContext));
        InstallTextResourcesTask installResourcesTask = new InstallTextResourcesTask("less install task", null, "UTF-8", ".*.less", "resources:processedLess", true, "ch.fastforward.processor.LessResourceModel", false);
        basicInstallTasks.add(installResourcesTask);
        return basicInstallTasks;
    }
}
